package com.testsolution;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testsolution.services.CalculateImpl;
import com.testsolution.util.ArchivosUtil;

@DisplayName("Test de Calculate")
public class CalculateImplTest {
	
	private CalculateImpl calculateImpl;
	
    @BeforeEach
    public void init(){
      calculateImpl = new CalculateImpl();
    }
    
    @DisplayName("Metodo para probar cuando al entrada no es null")
    @Test
    public void entradaNotNull(){
    	String valor  = calculateImpl.execute(ArchivosUtil.datosEntrada());
    	assertTrue(valor.equals(ArchivosUtil.datoSalida()));
    	assertEquals(valor, ArchivosUtil.datoSalida());
	}
    
    @DisplayName("Metodo para probar cuando al entrada es null")
    @Test
    public void entradaIsNull(){
    	String valor  = calculateImpl.execute(null);
    	assertTrue(valor.equals(ArchivosUtil.datoSalidaError()));
    	assertEquals(valor, ArchivosUtil.datoSalidaError());
	}
    
    @DisplayName("Metodo para probar cuando al entrada es null y arroja valor diferente")
    @Test
    public void entradaIsNullAndOutDifferent(){
    	String valor  = calculateImpl.execute(null);
    	assertFalse(valor.equals(ArchivosUtil.datoSalida()));
	}
    
    @DisplayName("Metodo para probar cuando al entrada tiene un error")
    @Test
    public void entradaError(){
    	String valor  = calculateImpl.execute(ArchivosUtil.datosEntradaError());
    	assertFalse(valor.equals(ArchivosUtil.datoSalida()));
	}
}
