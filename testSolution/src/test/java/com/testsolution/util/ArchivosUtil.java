package com.testsolution.util;

public abstract class ArchivosUtil {
	
	private ArchivosUtil() {}
	
	public static String datosEntrada() {
		String entrada = "2\r\n" + 
				"4 5\r\n" + 
				"UPDATE 2 2 2 4\r\n" + 
				"QUERY 1 1 1 3 3 3\r\n" + 
				"UPDATE 1 1 1 23\r\n" + 
				"QUERY 2 2 2 4 4 4\r\n" + 
				"QUERY 1 1 1 3 3 3\r\n" + 
				"2 4\r\n" + 
				"UPDATE 2 2 2 1\r\n" + 
				"QUERY 1 1 1 1 1 1\r\n" + 
				"QUERY 1 1 1 2 2 2\r\n" + 
				"QUERY 2 2 2 2 2 2";
		return entrada;
	}
	
	public static String datosEntradaError() {
		String entrada = "2\r\n" + 
				"4 5\r\n" + 
				"UPDATE 2 2 2\r\n" + 
				"QUERY 1 1 1 3 3 3\r\n" + 
				"UPDATE 1 1 1 23\r\n" + 
				"QUERY 2 2 2 4 4 4\r\n" + 
				"QUERY 1 1 1 3 3 3\r\n" + 
				"2 4\r\n" + 
				"UPDATE 2 2 2 1\r\n" + 
				"QUERY 1 1 1 1 1 1\r\n" + 
				"QUERY 1 1 1 2 2 2\r\n" + 
				"QUERY 2 2 2 2 2 2";
		return entrada;
	}
	
	public static String datoSalida() {
		String salida = "4\n" + 
				"4\n" + 
				"27\n" + 
				"0\n" + 
				"1\n" + 
				"1\n" + 
				"";
		return salida;
	}
	
	public static String datoSalidaError() {
		return "Se genero un problema con los datos de entrada";
	}
}
