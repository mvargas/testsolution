'use strict';

App.controller('Ctrlr', function($scope, $uibModal, Service) {
	var self = this;
	self.entrada='';
	self.salida='';
     
     self.calculate = function(entrada){
    	 Service.calculate(entrada)
	              .then(
	            		 function(d) {
	            			 self.salida=d.respuesta;
	  				    },
		              function(errResponse){
			               console.error('Error while calculated.'+errResponse);
		              }	
             );
     };
     
     self.submit = function() {
    	 self.salida='';
         console.log('Calculado', self.entrada);
         self.calculate(self.entrada);
     };
});