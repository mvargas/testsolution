package com.testsolution.dto;

import java.io.Serializable;

public class SolucionDto  implements Serializable{

	private static final long serialVersionUID = 2932738919808091809L;
	private String respuesta;

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
}