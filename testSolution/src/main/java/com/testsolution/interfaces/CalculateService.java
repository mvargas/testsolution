package com.testsolution.interfaces;

public interface CalculateService {
	public String execute(String entrada);
}