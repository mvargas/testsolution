package com.testsolution.services;

import java.util.Scanner;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Service;

import com.testsolution.interfaces.CalculateService;

@Service("CalculateService")
public class CalculateImpl implements CalculateService{
	
	private static final Logger LOGGER = Logger.getLogger(CalculateImpl.class);
    private long[][][] tree;
    private long[][][] nums;
    private int dimensions = 0;
    
	@Override
	public String execute(String entrada) {
        
		StringBuilder exit = new StringBuilder();
		try(Scanner ob = new Scanner(entrada)) {
	        int testcases = ob.nextInt();
	        ob.nextLine();
	        for (int i = 0; i < testcases; i++) {
	            String numsLine = ob.nextLine();
	            String[] numsLineParts = numsLine.trim().split(" ");
	            int dimension = Integer.parseInt(numsLineParts[0]);
	            int numOperations = Integer.parseInt(numsLineParts[1]);
	            
	            parametrization(dimension);            
	            for (int j = 0; j < numOperations; j++) {
	                String line = ob.nextLine();
	                String[] lineParts = line.split(" ");
	                if (lineParts[0].equals("UPDATE")) {
	                    this.update(Integer.valueOf(lineParts[1])-1, Integer.valueOf(lineParts[2])-1, Integer.valueOf(lineParts[3])-1, Integer.valueOf(lineParts[4]));
	                }
	                if (lineParts[0].equals("QUERY")) {
	                	exit.append(this.query(Integer.valueOf(lineParts[1])-1, Integer.valueOf(lineParts[2])-1, Integer.valueOf(lineParts[3])-1, Integer.valueOf(lineParts[4])-1, Integer.valueOf(lineParts[5])-1, Integer.valueOf(lineParts[6])-1)+"\n");
	                }
	            }
	        }
		} catch (Exception e) {
			LOGGER.info("Exception "+e);
			exit = new StringBuilder().append("Se genero un problema con los datos de entrada");
		}
		
        return exit.toString();
	}
	
    private void parametrization(int dimensions) {
        if (dimensions == 0) return;
        this.dimensions = dimensions;
        tree = new long[dimensions+1][dimensions+1][dimensions+1];
        nums = new long[dimensions][dimensions][dimensions];
    }
    
    private void update(int x, int y, int z, int value) {
        long delta = value - nums[x][y][z];
        nums[x][y][z] = value;
        for (int i = x + 1; i <= dimensions; i += i & (-i)) {
            for (int j = y + 1; j <= dimensions; j += j & (-j)) {
                for (int k = z + 1; k <= dimensions; k += k & (-k)) {
                    tree[i][j][k] +=  delta;
                }
            }
        }
    }
    
    private String query(int x1, int y1, int z1, int x2, int y2, int z2) {
        long result = sum(x2+1,y2+1,z2+1) - sum(x1,y1,z1) - sum(x1,y2+1,z2+1) - sum(x2+1,y1,z2+1) - sum(x2+1,y2+1,z1) + sum(x1,y1,z2+1) + sum(x1,y2+1,z1) + sum(x2+1,y1,z1);
        return String.valueOf(result);
    }
    
    private long sum(int x, int y, int z) {
        long sum = 0l;
        for (int i = x; i > 0; i -= i & (-i)) {
            for (int j = y; j > 0; j -= j & (-j)) {
                for (int k = z; k > 0; k -= k & (-k)) {
                    sum += tree[i][j][k];
                }
            }
        }
        return sum;
    }
}