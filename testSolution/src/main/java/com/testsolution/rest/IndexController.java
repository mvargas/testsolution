package com.testsolution.rest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.testsolution.dto.SolucionDto;
import com.testsolution.interfaces.CalculateService;

@Controller
public class IndexController {
	
	private static final Logger LOGGER = Logger.getLogger(IndexController.class);
	
	@Autowired
	CalculateService calculateService;
	
	
	@RequestMapping({"/", "/testSolution"})
	public String getIndexPage(){
		return "view/index";
	}
	
	@PostMapping(value = "/calculate/")
	public ResponseEntity<SolucionDto> calculate(@RequestBody String entrada, UriComponentsBuilder ucBuilder){
		
		LOGGER.info("Calculate "+entrada);
		
		SolucionDto solucion = new SolucionDto();
		solucion.setRespuesta(calculateService.execute(entrada));
		
		return new ResponseEntity<SolucionDto>(solucion, HttpStatus.OK) ;
	}
}